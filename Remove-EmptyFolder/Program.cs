﻿using CmdLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Remove_EmptyFolder
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CommandLineArguments arguments = CommandLine.Parse<CommandLineArguments>();

                if (arguments.Help)
                {
                    ShowHelp();
                }
                else
                {
                    // Now get to the job of recursively removing appropriate folders...
                    RemoveFolder(new DirectoryInfo(arguments.Path));
                }
            }
            catch (CommandLineException exception)
            {
                Console.WriteLine(exception.ArgumentHelp.Message);
                Console.WriteLine(exception.ArgumentHelp.GetHelpText(Console.BufferWidth));
                // Or show it in a message box if you like. 
            }
        }

        private static void ShowHelp()
        {
            Console.WriteLine("Remove-EmptyFolder");
            Console.WriteLine("");
            Console.WriteLine("Remove any empty folders recursively. Any folder which contains a file");
            Console.WriteLine("will not be removed (nor will its parent folders.)");
            Console.WriteLine("Copyright (c) 2015, Andrew Madden.");
        }

        static void RemoveFolder(System.IO.DirectoryInfo folder)
        {
            DirectoryInfo[] subFolders = folder.GetDirectories();
            if (subFolders.Length > 0)
            {
                foreach (DirectoryInfo subFolder in subFolders)
                    RemoveFolder(subFolder);
            }

            if (folder.GetFiles().Length > 0)
                return; // Files exist; cannot be deleted

            if (folder.GetDirectories().Length > 0)
                return;

            Console.WriteLine("Deleting folder [{0}]", folder.FullName);
            folder.Delete();


        }
    }
}
