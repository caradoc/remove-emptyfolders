﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CmdLine;

namespace Remove_EmptyFolder
{
    [CommandLineArguments(Program = "Remove-EmptyFolder", Title = "Remove empty folders", Description = "Remove empty folders (Recursively)")]
    public class CommandLineArguments
    {
        [CommandLineParameter(Command = "?", Default = false, Description = "Show Help", Name = "Help", IsHelp = true)]
        public bool Help { get; set; }

        [CommandLineParameter(Name = "path", ParameterIndex = 1, Required = true, Description = "Specifies the folder to be removed if empty.")]
        public string Path { get; set; }

        
    }
}
