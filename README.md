# README #

This repository contains the source code for a utility to remove empty folders. No files are deleted, only empty folders starting at the deepest levels. At this time, the folders being deleted are noted, although the folders being skipped are not.

### Requirements ###

* Windows (obviously)
* .NET 4.0 or higher

### Compilation Requirements ###

* Windows (obviously)
* .NET 4.0 or higher
* Visual Studio 2013 (Express should be OK)
* the .build file suits nAnt 0.91 or higher 

### How do I get set up? ###

* To begin using Remove-EmptyFolder, simply xcopy the contents to a folder on your path.
* There is no configuration
* Requires .NET 4.0 and CmdLine.DLL 2.8 (included)

### Who do I talk to? ###

* Repo owner: Andrew Madden (andrew.madden@hotmail.com)
