<#
.SYNOPSIS
Removes old backup files from the storage location

.DESCRIPTION
This cmdlet assumes that backups are stored in a standard location with a name like yyyy-mm-dd.bak.
This cmdlet will delete any backup files older than the specified age, EXCEPT that files for the 
first of each month are retained.

.PARAMETER Age
The number of days for which to retain backups (Default = 7)

.PARAMETER Path
The path in which to the backup files are located. (Current path is used if not provided.)

.EXAMPLE
Remove-OldFiles –Age 7 -Path "G:\Backups"
Remove any backup files older than 7 days in the G:\Backups folder.

.EXAMPLE 
Remove-OldFiles –Age 7
Remove any backup files older than 7 days in the current folder.

.NOTES
You need to run this function as a user with access to the location in which the backup files are stored.
#>

	param
	(
		[Parameter(Mandatory=$False,
		HelpMessage='How many daily backups to keep?')]
		[int]$Age = 7,
			
		[Parameter(Mandatory=$False,
		HelpMessage='Where are the backup files?')]
		[string]$Path
    )

Clear-Host
if ($Path -eq $null){
    $Path = Get-Location 
}
else {
Write-Host "Clearing $Path of old files"
}
$ComparisonDate = (Get-Date).AddDays(-7)
$filedate | Out-Null


function HandleFile ([System.IO.FileInfo]$file) {
    #Determine the date based on the file name
    #$filedate = GetFileDateFromName($file.Name)

    [string]::Format("Checking file:{0}", $file.Name)

    if ($file.LastWriteTime -lt (Get-Date).AddDays(-1 * $Age) -and ($file.LastWriteTime.Day -ne 1 ) ) {
        #[string]::Format("File {1}\{0} should be deleted", $file.Name, $file.Path) 
        Write-Host "Removing $file"
        Remove-Item "$file"
    }
}

Get-ChildItem $Path | ForEach-Object { HandleFile($_) }
