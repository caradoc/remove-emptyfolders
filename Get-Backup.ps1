<#
.SYNOPSIS
Gets the backup files from the network storage location

.DESCRIPTION
SQL Server database backups are stored in a standard network location. This cmdlet will restore the most recent
backup files for the nominated database (and related _Audit database as well if present). Any filename detail
such as server or date/time stamp are removed. 
NB: Only databases from TEMPURA are handled correctly at this time.

.PARAMETER Database
The name of the database to collect

.PARAMETER LocalPath
The path in which to place the backup files.

.PARAMETER NetworkPath
The network path in which the databases are stored.
The default is \\server\sqlbackups\TEMPURA

.PARAMETER NoAudit
If specified, the audit database backup file(s) are NOT copied locally for the restore.

.EXAMPLE
Get-Backup –database "Pubs" -localPath "G:\ProdBackups"
Get the database backup files for the Pubs database (and _Audit database).

.EXAMPLE 
Get-Backup -db NorthWind -NoAudit
Only get the files for the NorthWind database

.NOTES
You need to run this function as a user with access to the network location in which the backup files are stored.
#>

	param
	(
		[Parameter(Mandatory=$True,
		HelpMessage='What database is to be restored?')]
#		[Alias('db')]
		[string]$Database,
			
		[Parameter(Mandatory=$False,
#		ValueFromPipeline=$True,
#		ValueFromPipelineByPropertyName=$True,
		HelpMessage='Where should the database files be placed?')]
		[Alias('path')]
		[string]$LocalPath = 'G:\ProdBackups',
		
		[Parameter(Mandatory=$False,
#		ValueFromPipeline=$True,
#		ValueFromPipelineByPropertyName=$True,
		HelpMessage='Where are the database backup files stored?')]
		[Alias('source')]
		[string]$SourcePath = '\\server\sqlbackups\TEMPURA',
		
		[Parameter(Mandatory=$False,
		HelpMessage='If specified, the audit backup is not copied')]
		[switch]$NoAudit = $False
	)

	begin {
		del $LocalPath\$Database*.bak  # -ErrorActionSilentlyContinue
	}

	process {

		write-output "Beginning recovery process"

		# Copy main database file
		write-output "Copying main backup file for $Database"
		$source = get-childitem -Path $SourcePath\$Database\*.bak | Sort-Object -Property Name -Descending | Select-object -first 1
		# Format an elegant date/time stamp for the latest found backup. I think the backups are not FTP'd into place until AFTER 06:30 am
		$LastBackupDateTime = $source.LastWriteTime.ToShortDateString() + " " + $source.LastWriteTime.ToShortTimeString()
		write-output "Most recent backup found: $LastBackupDateTime"
		copy-item $source $LocalPath\$Database.bak

		if (!$NoAudit) {
			# Copy audit database file(s)
			$AuditDb = $Database + "_Audit"
			write-output "Copying backup files for $AuditDb"
			$BkpDate = get-date -format "yyyyMMdd"
			$source = get-childitem -Path $SourcePath\$AuditDb\*$AuditDb_$BkpDate*.bak  # Should get all backup files, even multipart, no matter how many
			copy-item $source -destination $LocalPath
			# clean up the file names so they work with the SQL job
			write-output "Cleaning up Audit file names (Removing Server name and date/time stamp)"
			dir $LocalPath\*$AuditDb*.bak | rename-item -newname { $_.name -replace "_$BkpDate......","" }
			dir $LocalPath\*$AuditDb*.bak | rename-item -newname { $_.name -replace "TEMPURA_","" } 
		} else {
			write-warning "Audit database is NOT BEING COPIED!"
			write-output "-NoAudit specified; Audit database not copied"
		}
	}
